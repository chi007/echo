
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.Set;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

@SuppressWarnings("restriction")
public class Echo {

	public final static int PORT = 8080;
	public final static String PATH = "/";
	
	public static void main(String[] args) throws Exception {
		HttpServer server = HttpServer.create(new InetSocketAddress(PORT), 0);
		server.createContext(PATH, new MyHandler());
		server.setExecutor(null); // creates a default executor
		server.start();
		System.out.println("Echo Server listening on PORT=" + PORT + " PATH=" + PATH + "...");
	}

	static class MyHandler implements HttpHandler {
		public void handle(HttpExchange t) throws IOException {
			StringBuilder sb = new StringBuilder("Request Details...\n\n");

			Headers headers = t.getRequestHeaders();
			Set<String> headerSet = headers.keySet();
			for (String headerKey : headerSet) {
				String out = headerKey + "=" + headers.get(headerKey);
				System.out.println(out);
				sb.append(out).append("\n");
			}

			String response = sb.toString();
			t.sendResponseHeaders(200, response.length());
			OutputStream os = t.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}

}
